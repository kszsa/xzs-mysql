import { post } from '@/utils/request'

export default {
  // pageList: query => post('/api/student/exampaper/answer/pageList', query),
  // answerSubmit: form => post('/api/student/exampaper/answer/answerSubmit', form),
  read: id => post('/api/admin/examPaperAnswer/read/' + id),
  edit: form => post('/api/admin/examPaperAnswer/edit', form)
}
